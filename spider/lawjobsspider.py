from urlparse import urljoin, urlparse

import re
from scrapy import Request
from scrapy.item import Item, Field
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity
from scrapy.spiders.crawl import CrawlSpider
from scrapylib.processors import default_input_processor, default_output_processor
from scrapy.selector import HtmlXPathSelector

__author__ = 'Agus'


class NormalizedJoin(object):
    """ Strips non-empty values and joins them with the given separator. """

    def __init__(self, separator=u' ', return_list=False):
        self.separator = separator
        self.return_list = return_list

    def __call__(self, values):
        result = self.separator.join(
            [value.strip() for value in values if value and not value.isspace()])
        if self.return_list:
            return [result]
        else:
            return result


class JobItem(Item):
    # required fields
    title = Field()
    # a unique id for the job on the crawled site.
    job_id = Field()
    # the url the job was crawled from
    url = Field()
    # name of the company where the job is.
    company = Field()

    # location of the job.
    # should ideally include city, state and country.
    # postal code if available.
    # does not need to include street information
    location = Field()
    description = Field()

    # the url users should be sent to for viewing the job. Sometimes
    # the "url" field requires a cookie to be set and this "apply_url" field will be differnt
    # since it requires no cookie or session state.
    apply_url = Field()

    # optional fields
    industry = Field()
    baseSalary = Field()
    benefits = Field()
    requirements = Field()
    skills = Field()
    work_hours = Field()


class JobItemLoader(ItemLoader):
    default_item_class = JobItem
    default_input_processor = default_input_processor
    default_output_processor = default_output_processor
    # all text fields are joined.
    description_in = Identity()
    description_out = NormalizedJoin()
    requirements_in = Identity()
    requirements_out = NormalizedJoin()
    skills_in = Identity()
    skills_out = NormalizedJoin()
    benefits_in = Identity()
    benefits_out = NormalizedJoin()


REF_REGEX = re.compile(r'\/(\d+)$')

APPEND_GB = lambda x: x.strip() + ", GB"


class SimplyLawJobs(CrawlSpider):
    """ Should navigate to the start_url, paginate through
    the search results pages and visit each job listed.
    For every job details page found, should produce a JobItem
    with the relevant fields populated.

    You can use the Rule system for CrawSpider (the base class)
    or you can manually paginate in the "parse" method that is called
    after the first page of search results is loaded from the start_url.

    There are some utilities above like "NormalizedJoin" and JobItemLoader
    to help making generating clean item data easier.
    """
    start_urls = ["http://www.simplylawjobs.com/jobs"]
    name = 'lawjobsspider'

    """Navigating All Pages"""
    def parse(self, response):
        sel = HtmlXPathSelector(response)
        sites = sel.xpath('//div[@id="pagination"]').extract()
        pageNumbers = [i for i in re.findall(r'>(.*?)<',str(sites)) if i.isdigit()]
        #l = ItemLoader(item=JobItem(), response=response)
        pageNumbers = map(int, pageNumbers)
        # uncomment this for fulldata 
        # last_page = max(pageNumbers)+1

        last_page=20
        for page in xrange(2,last_page):
            nextPage = urljoin("https://www.simplylawjobs.com/jobs", "?page"+str(page))
            print nextPage
            yield Request(nextPage,callback=self.parseJobs)

    """Extracting particular job data """
    def parseJobs(self, response1):
        jobItem = JobItem()
        parser = HtmlXPathSelector(response1)
        # details1 = sel1.select('//div[@class="info font-size-small"]/a[@class]/').extract()
        job_url_list = parser.xpath('//a[contains(@class,"view_job_btn")]/@href').extract()
        for job_url in job_url_list:
            url = "https://www.simplylawjobs.com"+str(job_url)
            yield Request(url,callback=self.parseJobsPage)
        # pagination_url_list = parser.xpath('//div[@id="pagination"]/a/@href').extract()
        # pagination_url = pagination_url_list[-1]
        # next_page_url = "https://www.simplylawjobs.com/jobs"+str(pagination_url)
        # print next_page_url
        # print pagination_url,"100"*100
        # raw_page_number = re.findall('\d+',pagination_url)
        # pagenumber = raw_page_number[0].strip() if raw_page_number else None
        # if pagenumber != 463:
        #     yield Request(url,callback=self.parseJobs)
    #     """for Extracting job description"""
    def parseJobsPage(self, response2):
        parser = HtmlXPathSelector(response2)
        # parsing

        raw_title = parser.xpath('//h1[@class="job_title"]//text()').extract()
        raw_job_id = re.findall('\/(\d+)',response2.url)
        raw_location = parser.xpath('//strong[contains(text(),"Location: ")]//following-sibling::a[1]//text()').extract()
        raw_company = parser.xpath('//h1[@class="job_title"]//parent::div//following-sibling::a//text()').extract()
        raw_salary = re.findall('"baseSalary": "(\w+)\s\"\,',response2.text)
        raw_industry = re.findall('"industry": "(.*)",',response2.text)
        raw_requirements = re.findall('"experienceRequirements": "(.*)",',response2.text)
        raw_benefits = re.findall('Company benefits: </strong>(.*)<',response2.text)
        raw_description = parser.xpath('//div[contains(@class,"description")]//text()').extract()
        raw_skills = re.findall('Essential skills and experience:(.*)Salary :',response2.text)
        # cleaning
        description = ' '.join(' '.join(raw_description).split()) if raw_description else []
        title = raw_title[0].strip() if raw_title else None
        job_id = raw_job_id[0] if raw_job_id else None
        url = response2.url
        # description = ' '.join(' '.join(description).split())
        location = raw_location[0].strip() if raw_location else None
        benefits = raw_benefits[0] if raw_benefits else None
        requirements = raw_requirements[0] if raw_requirements else None
        salary = raw_salary[0] if raw_salary else None
        industry = raw_industry[0] if raw_industry else None
        parser_skills = HtmlXPathSelector(raw_skills[0]) if raw_skills else None
        li_skills = parser_skills.xpath('//text()').extract() if parser_skills else None
        skills = "".join(li_skills) if li_skills else None
        company = raw_company[0] if raw_company else None

        items = {
                    "description":description,
                    "title":title,
                    "job_id":job_id,
                    "url":url,
                    "location":location,
                    "benefits":benefits,
                    "requirements":requirements,
                    "baseSalary":salary,
                    "industry":industry,
                    "skills":skills,
                    "apply_url":response2.url,
                    "company":company,
                    "work_hours":'',
                }
        return items